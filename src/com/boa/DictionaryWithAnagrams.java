package com.boa;
import java.util.ArrayList;
import java.util.Iterator;

public class DictionaryWithAnagrams {

	public static void main(String[] args) {
		ArrayList<String> wordsInDictionary = new ArrayList<String>();
		wordsInDictionary.add("lion");
		wordsInDictionary.add("veto");
		wordsInDictionary.add("first");
		wordsInDictionary.add("tab");
		wordsInDictionary.add("ionl");
		wordsInDictionary.add("noli");
		wordsInDictionary.add("act");
		wordsInDictionary.add("tac");

		ArrayList<String> dictionary = findAllAnagramsInDictionary(wordsInDictionary
				.iterator());

		findAllAnagramsForWord("lion", dictionary);
		findAllAnagramsForWord("a", dictionary);
		findAllAnagramsForWord("bat", dictionary);
		findAllAnagramsForWord("cat", dictionary);
		findAllAnagramsForWord("vote", dictionary);

	}

	public static ArrayList<String> findAllAnagramsInDictionary(
			Iterator<String> dictionary) {
		int i = 0;
		ArrayList<String> dictionaryList = new ArrayList<String>();
		System.out.println("Words present in the Dictionary are ");
		while (dictionary.hasNext()) {
			String string = dictionary.next();
			System.out.println(++i + ". " + string);
			dictionaryList.add(string);
		}

		return dictionaryList;
	}

	public static ArrayList<String> findAllAnagramsForWord(String word,
			ArrayList<String> dictionaryList) {

		ArrayList<String> anagramsList = new ArrayList<String>();
		ArrayList<String> anagramWordsInDictionary = new ArrayList<String>();
		
		// Returns all the anagrams for a particular word
		anagramsList = getanagramsList(word);
		System.out.println("Anagrams for the Word '" + word
				+ "' Present in the Dictionary are ");
		
		// if the given dictionary contains the input word with all anagrams then it prints
		for (String inputWord : anagramsList) {
			if (dictionaryList.contains(inputWord)) {
				System.out.print("{ '" + inputWord + "' }");
				anagramWordsInDictionary.add(inputWord);
			}
		}
		System.out.println("\n");

		return anagramWordsInDictionary;
	}

	/*
	 * This method will find all the anagrams for a given particular word
	 */

	public static ArrayList<String> getanagramsList(String s) {
		ArrayList<String> permutations = new ArrayList<String>();
		if (s == null) {

			return null;
		} else if (s.length() == 0) {
			permutations.add("");
			return permutations;
		}

		char first = s.charAt(0);

		String remainder = s.substring(1);

		ArrayList<String> words = getanagramsList(remainder);

		for (String word : words) {
			for (int j = 0; j <= word.length(); j++) {
				permutations.add(insertCharAt(word, first, j));
			}
		}
		return permutations;
	}

	public static String insertCharAt(String word, char c, int i) {
		String start = word.substring(0, i);
		String end = word.substring(i);
		return start + c + end;
	}

}
